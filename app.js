const express = require('express');
const bodyParser = require('body-parser');
// const cors = require('cors');

const app = express();
// app.use(cors());

const userRoute = require('./routes/user.routing');
const MessageRoute = require('./routes/message.routing');

app.use(bodyParser.json());

app.use("/api/user", userRoute);
app.use("/api/message", MessageRoute);

module.exports = app;