const express = require('express');
const messageController = require('../controllers/message.controller');
const checkAuthMiddleWare = require('../middleware/check-auth');

const router = express.Router();

router.post("/send", checkAuthMiddleWare.checkAuth, messageController.send);
router.get("/messages/:Id_Sender/:Id_Recipient", checkAuthMiddleWare.checkAuth, messageController.messagesById);

module.exports = router;