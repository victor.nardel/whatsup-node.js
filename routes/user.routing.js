const express = require('express');
const userController = require('../controllers/user.controller');
const checkAuthMiddleWare = require('../middleware/check-auth');

const router = express.Router();

router.post("/create", userController.signUp);
router.post("/login", userController.login);
router.get("/users", checkAuthMiddleWare.checkAuth, userController.getAllUsersConnected);
router.post("/isconnected", checkAuthMiddleWare.checkAuth, userController.patchConnected);

module.exports = router;