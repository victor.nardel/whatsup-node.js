const models = require('../models');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Validator = require('fastest-validator');

async function signUp(req, res) {

    try {
        const emailResult = await models.User.findOne({ where: { Email: req.body.Email } });

        if (emailResult) {
            return res.status(409).json({ Message: "Email already exists" });
        }

        const usernameResult = await models.User.findOne({ where: { Username: req.body.Username } });

        if (usernameResult) {
            return res.status(409).json({ Message: "Username already exists" });
        }

        if (!req.body.Password) return res.status(400).json({ Message: "Tous les champs obligatoires doivent être remplis" });

        const salt = await bcryptjs.genSalt(10);
        const hash = await bcryptjs.hash(req.body.Password, salt);

        const user = {
            Username: req.body.Username,
            FirstName: req.body.FirstName,
            LastName: req.body.LastName,
            Email: req.body.Email,
            Password: hash
        };

        const schema = {
            Username: {type: "string", optional: false, max: "100"},
            Email: {type: "string", optional: false, max: "100"},
            Password: {type: "string", optional: false, max: "1000"},
        }
    
        const v = new Validator();
        const validateResponse = v.validate(user, schema);
    
        if (validateResponse !== true) {
            return res.status(400).json({
                Message: "Bad object sent",
                error: validateResponse
            });
        }

        const createdUser = await models.User.create(user);

        res.status(201).json({
            Message: "User created successfully",
            createdUser
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ Message: "Something went wrong", error: err });
    }
}

function login(req, res) {
    models.User.findOne({ where: { Email: req.body.Email } })
        .then(user => {
            if (user === null) {
                res.status(401).json({
                    Message: "Invalid credentials",
                });
            } else {
                bcryptjs.compare(req.body.Password, user.Password, (err, result) => {
                    if (result) {
                        const token = jwt.sign({
                            Email: user.Email,
                            UserId: user.id,
                        }, process.env.JWT_KEY, (err, token) => {
                            res.status(200).json({
                                Message: "Authentication sucessful",
                                Jwt: token,
                                UserId: user.id,
                                Username: user.Username
                            });
                        });
                    } else {
                        res.status(401).json({
                            Message: "Invalid credentials",
                        });
                    }
                })
            }
        }).catch(err => {
            res.status(500).json({
                Message: "Something went wrong",
            });
        });
}

function patchConnected(req, res) {

    const patchUserConnected = {
        Connected: req.body.Connected,
    };

    const schema = {
        Connected: {type: "boolean", optional: false},
    }

    const v = new Validator();
    const validateResponse = v.validate(patchUserConnected, schema);

    if (validateResponse !== true) {
        return res.status(400).json({
            message: "Bad object sent!",
            error: validateResponse
        });
    }

    models.User.update(patchUserConnected, {where: {id: req.userData.UserId}}).then(result => {
        res.status(200).json({
            message: "Post updated successfully",
        });
    }).catch(err => {
        res.status(500).json({
            message: "Something went wrong",
            error: err
        });
    });
}

function getAllUsersConnected(req, res) {
    models.User.findAll({
        where: {
            Connected: true
        },
    }).then(result => {
        res.status(200).json({
            result: result
        });
    }).catch(err => {
        res.status(500).json({
            Message: "Something went wrong",
            error: err
        });
    });

}

module.exports = {
    signUp: signUp,
    login: login,
    patchConnected: patchConnected,
    getAllUsersConnected: getAllUsersConnected
};