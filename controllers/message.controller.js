const models = require('../models');
const Validator = require('fastest-validator');
const { Op } = require('sequelize');

function send(req, res) {
    const sendingMsg = {
        Content: req.body.Content,
        Id_Sender: req.body.Id_Sender,
        Id_Recipient: req.body.Id_Recipient,
    };

    const schema = {
        Content: {type: "string", optional: false, max: "1000"},
        Id_Sender: {type: "number", optional: false},
        Id_Recipient: {type: "number", optional: false}
    }

    const v = new Validator();
    const validateResponse = v.validate(sendingMsg, schema);

    if (validateResponse !== true) {
        return res.status(400).json({
            message: "Bad object sent",
            error: validateResponse
        });
    }

    models.User.findByPk(req.body.Id_Recipient).then(recipientIdExist => {
        if (recipientIdExist !== null) {
            models.User.findByPk(req.body.Id_Sender).then(senderIdExist => {
                if (senderIdExist !== null) {

                    models.Message.create(sendingMsg).then(result => {

                        const MsgCount = {
                            Id_Sender: req.body.Id_Sender,
                            Id_Recipient: req.body.Id_Recipient,
                            MsgCount: 1
                        }
                        
                        

                        models.User_User.findOrCreate({
                            where: {
                              Id_Sender: req.body.Id_Sender,
                              Id_Recipient: req.body.Id_Recipient
                            },
                            defaults: MsgCount
                          }).then(([msgCount, created]) => {
                        
                            if (!created)
                                msgCount.increment('MsgCount', { by: 1 })
                        
                            res.status(201).json({
                                Message: "Message sent successfully",
                                sendingMsg: result
                            });
                        }).catch((err) => {
                            res.status(500).json({
                                message: "Something went wrong",
                                error: err
                            });
                        });
                    }).catch(err => {
                        res.status(500).json({
                            message: "Something went wrong",
                            error: err
                        });
                    });
                } else {
                    res.status(400).json({
                        message: "Invalid SenderId",
                    });
                }
            });
        } else {
            res.status(400).json({
                message: "Invalid RecipientId",
            });
        }
    });
}

function messagesById(req, res) {
    models.Message.findAll({
        where: {
            [Op.or]: [
                {
                    Id_Sender: req.params.Id_Sender,
                    Id_Recipient: req.params.Id_Recipient
                },
                {
                    Id_Sender: req.params.Id_Recipient,
                    Id_Recipient: req.params.Id_Sender
                }
            ]
        },
        order: [['createdAt', 'ASC']]
      }).then(messages => {
        res.status(200).json({
            Messages: messages
        });
      });

}

module.exports = {
    send: send,
    messagesById: messagesById
};