const app = require('./app')
const server = require('http').createServer(app);
const WebSocket = require('ws');
const models = require('./models');
const port = 5000;

// ------------ Web Socket --------------
const wss = new WebSocket.Server({ server: server, path: '/ws/open' });
var connectionMap = new Map();

wss.on('connection', (ws, req) => {
  const urlParts = new URL(req.url, `http://${req.headers.host}`);
  const userId = urlParts.searchParams.get('userId');

  if (!userId) {
    ws.close();
    return;
  }

  connectionMap.set(
    Number(userId), 
    { ws: ws, lastUsed: new Date() }
  );

  ws.on('message', (message) => {
    try {
      const msg = JSON.parse(message.toString());

      switch (msg.Type) {
        case 'Private':
          if (msg.Id_Recipient && msg.Id_Recipient !== msg.Id_Sender) {
            const recipientConnection = connectionMap.get(msg.Id_Recipient);
            
            if (recipientConnection)
              recipientConnection.ws.send(message);
          }
          break;
        case 'Connected':
          connectionMap.forEach((connection, key) => {
            if (key != msg.id)
              connection.ws.send(message);
          });
          break;
        case 'IsWriting':
          if (msg.Id_Recipient && msg.Id_Recipient !== msg.Id_Sender) {
            const recipientConnection = connectionMap.get(msg.Id_Recipient);
            
            if (recipientConnection)
              recipientConnection.ws.send(message);
          }
          break;

        default:
          console.error('Unknown message type:', msg.Type);
      }

    } catch (err) {
      console.error('Error parsing or processing message:', err);
    }
  });

  ws.on('close', (e) => {
    models.User.update({Connected: false}, {where: {id: userId}}).then(result => {
    }).catch(err => {
        console.log(err);
    });

    const json = JSON.stringify({Type: 'Disconnected', id: userId});
    connectionMap.forEach((connection) => {
      connection.ws.send(Buffer.from(json));
    });
  });
});

// --------------------------------------

server.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});