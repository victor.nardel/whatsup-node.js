'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User_User.init({
    Id_Sender: DataTypes.INTEGER,
    Id_Recipient: DataTypes.INTEGER,
    MsgCount: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_User',
  });
  return User_User;
};